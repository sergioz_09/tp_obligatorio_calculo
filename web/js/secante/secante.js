$(window).on('load', function() {
    /**
     * Carga los datos del enunciado.
     */
    function cargarDatos() {
        $("#x0").val("40");
        $("#x1").val("45");
        $("#error").val("0.05");
        $("#function_params").val("v");
        $("#function_content").val("return (1/0.2048)*(30039*Math.log(1 + (0.08*v/57))-(42.16*v))+300;");
    }
    cargarDatos();
    $(".nav").find(".active").parent().removeClass("active").addClass("active"); // fix for the active state of nav-bar


    /**
     * Crea una funcion con los datos ingresados en pantalla.
     * @returns {*}
     */
    function getFunction() {
        return new Function($("#function_params").val().split(','), $("#function_content").val());
    }

    /**
     * Ejecuta el metodo de la secante y muestra el resultado.
     */
    $("#execute").click(function() {
        var a = parseFloat($("#x0").val());
        var b = parseFloat($("#x1").val());
        var e = parseFloat($("#error").val());
        var fn = getFunction();
        $("#resultado").val(secante(a, b , e, fn));
    });

    /**
     * Metodo de la secante.
     * @param x0
     * @param x1
     * @param error
     * @param f
     * @return
     */
    function secante(x0, x1, error, f) {
        var x2 = x1 -  ((f(x1) * (x1 - x0)) / ((f(x1) - f(x0)))); // obtenemos el valor de la raiz x2
        if(Math.abs(f(x2)) > error) {
            x2 = secante(x1, x2, error, f);
        }
        return x2;
    }
    
});
