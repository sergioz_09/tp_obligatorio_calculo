/**
 * Created by sergio on 9/10/16.
 * Este archivo contiene el algoritmo de interpolacion de newton.
 */

/**
 * Algoritmo de interpolacion de newton;
 * @param grado
 * @param x
 * @return
 */
function newton(grado, x) {
    var resultado = 0;
    var posX0 = 0; // posicion del x0
    // busca la posicion de xo
    while (posX0 < puntos.length - 2 && !( puntos[posX0].x < x && puntos[posX0 + 1].x > x)) {
        posX0++;
    }
    //segun el grado del polinomio validar si esta disponible en la tabla x0 + grado
    if(puntos.length - 1 > posX0 + grado) {
        resultado = polinomio(grado, x, posX0);
    } else {
        resultado = undefined;
    }
    return resultado;
}

/**
 *
 * @param grado
 * @param x
 * @param posX0
 * @return
 */
function polinomio(grado, x, posX0) {
    var resultado = 0;
    // armamos la expresion general del polinomio
    while (grado >= 0) {
        if(grado == 0) {
            resultado = resultado + puntos[posX0].fx;
        } else {
            //(x − x0) * (x − x1) ... (x − xn-1)
            var mult = 1;
            for(var i = 0; i < grado; i++) {
                mult = mult * (x - puntos[posX0 + i].x);
            }
            resultado = resultado + mult *  diferenciaDividida(grado, x, posX0);
        }
        grado--;
    }
    return resultado;
}

/**
 * Resuelve una diferencia dividida de x variables [x1,x2, ... , xn]
 * @param grado
 * @param x
 * @param posX0
 * @return
 */
function diferenciaDividida(grado, x, posX0) {
    var resultado = 0;
    if (grado == 1) { // fx1 -fx0 / x1 - x0
        resultado = puntos[posX0 + 1].fx - puntos[posX0].fx / (puntos[posX0 + 1].x - puntos[posX0].x);
    } else {
        resultado = (diferenciaDividida(grado - 1, x, posX0 + 1) - diferenciaDividida(grado - 1, x, posX0)) / (puntos[posX0 + grado].x - puntos[posX0].x);
    }
    return resultado;
}