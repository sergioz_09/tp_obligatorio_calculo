/**
 * Created by Sergio on 10/09/2016.
 * Este archivo contiene la logica de la interfaz visual.
 * require del archivo polinomio_newton para que funcione el metodo de ejecutar.
 */
var puntos = [];// contiene el listado de los puntos
$(window).on('load', function() {
    cargarDatosEnunciado();

    /**
     * Carga los valores del enunciado
     */
    function cargarDatosEnunciado() {
        $("#punto").val("3.5");
        agregarPunto(0, 0);
        agregarPunto(1, 0.7);
        agregarPunto(2, 2.4);
        agregarPunto(3, 3.1);
        agregarPunto(4, 4.2);
        agregarPunto(5, 4.8);
        agregarPunto(6, 5.7);
        agregarPunto(7, 5.9);
        agregarPunto(8, 6.2);
        agregarPunto(9, 6.4);
        agregarPunto(10, 6.4);
        refrescarTabla();
    }

    /**
     * Agrega un punto a la lista
     * @param x
     * @param fx
     */
    function agregarPunto(x, fx) {
        puntos.push({x: x, fx: fx})
    }

    // elimina el punto de la lista
    /**
     * Elimina un punto de la lista
     * @param index La posicion del elemento
     */
    function eliminarPunto(index) {
        pAux = []
        puntos.forEach(function(obj, i) {
           if(i != index) {
               pAux.push(obj)
           }
        });
        puntos = pAux;
    }

    /**
     * Ordena la lista de puntos segun x
     */
    function ordenarPuntos() {
        puntos.sort(function (p1, p2) {
            return p1.x > p2.x
        });
    }

    /**
     * Refresca la tabla de puntos
     * ordena la lista y los redibuja en orden.
     */
    function refrescarTabla() {
        // ordena la lista de puntos
        //ordenarPuntos();
        // vacia el contenedor
        $(".puntos-contenedor").html("");
        // Recorre la lista de puntos y usando el template lo agrega cada item a la tabla.
        puntos.forEach(function(obj, i) {
            var template = $('#hidden-template').html();
            template = template.replace("valx", obj.x).replace("valfx", obj.fx).replace("puntoIndex", i);
            $(".puntos-contenedor").append(template);
        });
    }

    /**
     * Cuando el elemento con id "agregarPunto" es clickeado se ejecuta la rutina de agregar elemento.
     */
    $("#agregarPunto").click(function() {
        agregarPunto(parseFloat($("#x").val()), parseFloat($("#fx").val()));
        $("#x, #fx").val(""); // limpia los inputs
        refrescarTabla();
    });

    /**
     * Cuando algun elemento con la clase eliminarPunto es cliqueado, obtiene la posicion del elemento en la lista de puntos
     * lo elimina y redibuja la tabla.
     */
    $(document).on("click", ".eliminarPunto", function(e) {
        eliminarPunto($(e.target).data("index")); // Remueve el elemento de la lista de puntos
        refrescarTabla(); // re-dibuja la lista
    });

    /**
     * Ejecuta el metodo de newton
     */
    $("#execute").click(function() {
        var grado = parseFloat($("#grado").val());
        var punto = parseFloat($("#punto").val());
        $("#resultado").val(newton(grado, punto));
    });
});
