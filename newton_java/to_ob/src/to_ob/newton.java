package tp.newton;

import java.util.ArrayList;

public class newton {
	// los puntos precargados.
	private static Punto[] puntos = {
		new Punto(0, 0),
		new Punto(1, 0.7),
		new Punto(2, 2.4),
		new Punto(3, 3.1),
		new Punto(4, 4.2),
		new Punto(5, 4.8),
		new Punto(6, 5.7),
		new Punto(7, 5.9),
		new Punto(8, 6.2),
		new Punto(9, 6.4),
		new Punto(10, 6.4),
	};

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int grado = 2;
		double x = 3.5;
		System.out.println(newton(grado, x));
	}

	/**
	 * 
	 * @param grado
	 * @param x
	 * @return
	 */
	public static double newton(int grado, double x) {
		double resultado = 0;
		int x0 = 0; // posicion del x0		
		// busca la posicion de xo
		while (x0 < puntos.length - 2 && !( puntos[x0].x < x && puntos[x0 + 1].x > x)) {
			x0++;
		}
		//segun el grado del polinomio validar si esta disponible en la tabla x0 + grado
		if(puntos.length - 1 > x0 + grado) {
			resultado = polinomio(grado, x, x0);
		} else {
			// no es posible aplicar newton, el grado supera los datos.
			resultado = -1;
		}
		return resultado;
	}
	
	/**
	 * 
	 * @param grado
	 * @param x
	 * @param posX0
	 * @return
	 */
	public static double polinomio(int grado, double x, int posX0) {
		double resultado = 0;
		// armamos la expresion general del polinomio
		while (grado >= 0) {
			if(grado == 0) {
				resultado = resultado + puntos[posX0].fx;
			} else {
				//(x − x0) * (x − x1) ... (x − xn-1) 
				double mult = 1;
				for(int i = 0; i < grado; i++) {
					mult = mult * ( x - puntos[posX0 + i].x);
				}
				resultado = resultado + mult *  diferenciaDividida(grado, x, posX0);
			}
			grado--;
		}
		return resultado;
	}

	/**
	 * 
	 * @param grado
	 * @param x
	 * @param posX0
	 * @return
	 */
	public static double diferenciaDividida(int grado, double x, int posX0) {
		double resultado = 0;
		if (grado == 1) { // fx1 -fx0 / x1 - x0
			resultado = puntos[posX0 + 1].fx - puntos[posX0].fx / (puntos[posX0 + 1].x - puntos[posX0].x);
		} else {
			resultado = (diferenciaDividida(grado - 1, x, posX0 + 1) - diferenciaDividida(grado - 1, x, posX0)) / (puntos[posX0 + grado ].x - puntos[posX0].x);
		}
		return resultado;
	}

}
